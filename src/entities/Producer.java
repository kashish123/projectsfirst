package entities;

import java.util.LinkedList;
import java.util.Random;
//import static threadexample.Threadexample.queue;

public class Producer extends Thread 
{

    private boolean running = true;
    int producerNum;
    Random r = new Random();
    LinkedList<Integer> queue = new LinkedList<>();

    public Producer(LinkedList<Integer> queue) {
        this.queue = queue;
    }

    public void produce() 
    {
        synchronized (queue) 
        {
            while (queue.size() >= 20) 
            {
                System.out.println("List is Full");
                try 
                {
                    queue.wait();
                }
                catch (InterruptedException e) 
                {
                    System.out.println("Producer Wait as queue full" + e);
                }
            }
            producerNum = r.nextInt(10);
            System.out.println("Produced  " + producerNum);

            queue.add(producerNum);

            queue.notify();
        }
    }

    @Override
    public void run() 
    {
        System.out.println("Production starts ");

        while (running) 
        {
            produce();

        }
    }

    public void shutdown() 
    {
        running = false;
    }

}
