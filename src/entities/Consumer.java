package entities;

import java.util.LinkedList;
//import static threadexample.Threadexample.queue;

public class Consumer extends Thread 
{

    LinkedList<Integer> queue = new LinkedList<>();
    private boolean running = true;

    public Consumer(LinkedList<Integer> queue) {
        this.queue = queue;
    }

    public void consume() 
    {
        synchronized (queue) 
        {
            while (queue.isEmpty()) 
            {
                System.out.println("List is empty" + queue + queue.size());
                try 
                {
                    queue.wait();

                } 
                catch (InterruptedException e) 
                {
                    System.out.println(e);
                }
            }

            int itemConsumed = queue.remove(0);
            queue.notify();
            System.out.println("consumed  " + itemConsumed);
        }

    }

    @Override
    public void run() 
    {
        System.out.println("New consumer is started");

        while (running) 
        {
            consume();
        }
    }

    public void shutdown() 
    {
        running = false;
    }
}
