

package threadexample;

import entities.Consumer;
import entities.Producer;
import java.util.LinkedList;
import java.util.Scanner;

public class Threadexample
{

    public static final LinkedList<Integer> queue = new LinkedList<>();

    public static void main(String[] args) 
    {
        Scanner p = new Scanner(System.in);
        Producer produceObject = new Producer(queue);
        Consumer consumerObject = new Consumer(queue);
        Thread prodTh = new Thread(produceObject);
        Thread consTh = new Thread(consumerObject);
        prodTh.start();
        consTh.start();
        
        p.nextLine();
        produceObject.shutdown();
        consumerObject.shutdown();
        
        

    }
}
